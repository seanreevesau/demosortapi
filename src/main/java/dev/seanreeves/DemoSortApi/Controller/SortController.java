package dev.seanreeves.DemoSortApi.Controller;

import dev.seanreeves.DemoSortApi.Models.SortRequest;
import dev.seanreeves.DemoSortApi.Models.SortResponse;
import dev.seanreeves.DemoSortApi.Service.SortService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SortController {

    @Autowired
    SortService sortService;

    @ApiOperation(value = "Sorts an array of integers whilst also returning the sum", response = SortResponse.class)
    @PostMapping("/sort")
    SortResponse sort(
            @RequestBody SortRequest body,
            @RequestParam(defaultValue = "false",name = "Reverse Order") boolean reverseOrder) {
        return sortService.sort(body.integers, reverseOrder);
    }
}
