package dev.seanreeves.DemoSortApi.Models;

import java.util.List;

public class SortResponse {
    public List<Integer> sortedIntegers;
    public int sum;

    public SortResponse(List<Integer> sortedIntegers, int sum) {
        this.sortedIntegers = sortedIntegers;
        this.sum = sum;
    }
}
