package dev.seanreeves.DemoSortApi.Service;

import dev.seanreeves.DemoSortApi.Models.SortResponse;

import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SortService {

    public SortResponse sort(List<Integer> unsortedList, boolean reverseOrder) {
        List<Integer> sortedList = unsortedList.stream()
                .sorted((i1, i2) -> reverseOrder ? i2.compareTo(i1) : i1.compareTo(i2))
                .collect(Collectors.toList());

        int sum = unsortedList.stream()
                .reduce(0, (i1, i2) -> (i1 + i2));
        return new SortResponse(sortedList, sum);
    }
}
