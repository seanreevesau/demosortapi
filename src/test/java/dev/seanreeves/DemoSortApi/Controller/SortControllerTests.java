package dev.seanreeves.DemoSortApi.Controller;

import dev.seanreeves.DemoSortApi.Models.SortResponse;
import dev.seanreeves.DemoSortApi.Service.SortService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;

@WebMvcTest(value = SortController.class)
public class SortControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SortService sortService;

    private static final String url = "/api/sort";

    @BeforeEach
    public void setup(){
        Mockito.when(sortService.sort(anyList(), anyBoolean())).thenReturn(new SortResponse(List.of(1,2,3), 6));
    }

    @Test
    public void sort_ReturnsOK_OnValidRequest() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"integers\": [1,2,3]}");
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    public void sort_ReturnsBadRequest_OnEmptyRequestBody() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    public void sort_ReturnsBadRequest_OnMalformedJSON() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"integers: [1,2,3]");
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    public void sort_ReturnsUnsupportedMediaType_OnInvalidMediaType() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .content("{\"integers\": [1,2,3]}");
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(415, result.getResponse().getStatus());
    }
}
