package dev.seanreeves.DemoSortApi.Service;

import dev.seanreeves.DemoSortApi.Models.SortResponse;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@SpringBootTest
public class SortServiceTests {

    @Autowired
    private SortService _sut;

    @Test
    public void sort_ReturnsCorrectSum() {
        SortResponse response = _sut.sort(List.of(-1, 0, 1, 10), false);
        assertEquals(10, response.sum);
    }

    @ParameterizedTest
    @MethodSource("sortTestParameters")
    public void sort_ReturnsOrderedList_WhenReversedIsFalse(List<Integer> unsortedList, List<Integer> sortedList, int sum) {
        SortResponse response = _sut.sort(unsortedList, false);

        assertEquals(sum, response.sum);
        assertEquals(sortedList, response.sortedIntegers);
    }

    @ParameterizedTest
    @MethodSource("reverseSortTestParameters")
    public void sort_ReturnsReversedList_WhenReversedIsTrue(List<Integer> unsortedList, List<Integer> sortedList, int sum) {
        SortResponse response = _sut.sort(unsortedList, true);

        assertEquals(sum, response.sum);
        assertEquals(sortedList, response.sortedIntegers);
    }

    private static Stream<Arguments> sortTestParameters() {
        return Stream.of(
                arguments(List.of(1), List.of(1), 1),
                arguments(List.of(3,2,1), List.of(1,2,3), 6),
                arguments(List.of(-3,-2,-1), List.of(-3, -2, -1), -6)
        );
    }

    private static Stream<Arguments> reverseSortTestParameters() {
        return Stream.of(
                arguments(List.of(1), List.of(1), 1),
                arguments(List.of(1,2,3), List.of(3,2,1), 6),
                arguments(List.of(-1,-2,-3), List.of(-1, -2, -3), -6)
        );
    }
}
